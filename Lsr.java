import java.io.*;
import java.net.*;
import java.util.*;

public class Lsr {
	
    private char id;
    private int port;
    private int neighbour_num;
    private ArrayList<String> adjacent_nodes;

    public Lsr () {}

    public static void main (String[] args) {
        Lsr l = new Lsr();

        if (args.length != 3) {
            System.out.println("Required arguments: NODE_ID NODE_PORT config.txt");
            return;
        }

        l.id = args[0];
        l.port = Integer.parseInt(args[1]);

        try {
            BufferedReader reader = new BufferedReader(new FileReader(args[2]));
            while ((String line = reader.readLine()) != null) {
                l.adjacent_nodes.add(line);
            }
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}